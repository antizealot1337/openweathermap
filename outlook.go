package openweathermap

import "encoding/json"

// WeatherInfo is the forecsted weather plus a description.
type WeatherInfo struct {
	ID          int
	Type        string `json:"main"`
	Description string
	Icon        string
} //struct Weather

// String returns the WeatherInfo.Type + space + WeatherInfo.Description.
func (w WeatherInfo) String() string {
	return w.Type + " " + w.Description
} //String

// City for the Outlook.
type City struct {
	Coord struct {
		Lat float64
		Lon float64
	}
	Country    string
	ID         int
	Name       string
	Population int
} //struct City

// HourlyForecast represents a forecast for the HourlyOutlook.
type HourlyForecast struct {
	UnixStamp  int64  `json:"dt"`
	TimeString string `json:"dt_txt"`
	Main       struct {
		Humidity int
		Pressure float32
		Temp     Temperature
	}
	Weather []WeatherInfo
	Clouds  struct {
		All int
	}
	Wind struct {
		Speed     float32
		Direction float32 `json:"deg"`
	}
} //HourlyForecast

// HourlyOutlook is the forecated hourly outlook.
type HourlyOutlook struct {
	City      `json:"city"`
	Forecasts []HourlyForecast `json:"list"`
} //struct HourlyOutlook

// DailyForecast is a forecast for the DailyOutlook.
type DailyForecast struct {
	UnixStamp int64 `json:"dt"`
	Humidity  int
	Pressure  float32
	Rain      float32
	Weather   []WeatherInfo
	Temp      struct {
		Day     Temperature
		Evening Temperature `json:"eve"`
		Max     Temperature
		Min     Temperature
		Morning Temperature `json:"morn"`
		Night   Temperature
	}
	WindSpeed     float32 `json:"speed"`
	WindDirection int     `json:"deg"`
	Clouds        int
} //DailyForecast

// DailyOutlook is th forecated daily outlook.
type DailyOutlook struct {
	City      `json:"city"`
	Forecasts []DailyForecast `json:"list"`
} //struct DailyOutlook

func unmarshalHourly(jsonData []byte) (*HourlyOutlook, error) {
	var hourly HourlyOutlook

	// Unmarshal an HourlyOutlook
	err := json.Unmarshal(jsonData, &hourly)

	return &hourly, err
} //UnmarshalHourly

func unmarshalDaily(jsonData []byte) (*DailyOutlook, error) {
	var daily DailyOutlook

	// Unmarshal the DailyOutlook
	err := json.Unmarshal(jsonData, &daily)

	return &daily, err
} //UnmarshalDaily
