package openweathermap

import "testing"

const (
	hourlyJSON = `{
    "city": {
        "coord": {
            "lat": 34.966671,
            "lon": 138.933334
        },
        "country": "JP",
        "id": 1851632,
        "name": "Shuzenji",
        "population": 0,
        "sys": {
            "population": 0
        }
    },
    "cnt": 40,
    "cod": "200",
    "list": [
        {
            "clouds": {
                "all": 8
            },
            "dt": 1450915200,
            "dt_txt": "2015-12-24 00:00:00",
            "main": {
                "grnd_level": 930.3,
                "humidity": 96,
                "pressure": 930.3,
                "sea_level": 1027.76,
                "temp": 281.64,
                "temp_kf": 2.63,
                "temp_max": 281.64,
                "temp_min": 279.012
            },
            "rain": {
                "3h": 0.1325
            },
            "sys": {
                "pod": "d"
            },
            "weather": [
                {
                    "description": "light rain",
                    "icon": "10d",
                    "id": 500,
                    "main": "Rain"
                }
            ],
            "wind": {
                "deg": 338.502,
                "speed": 0.97
            }
        }
    ],
    "message": 0.0044
}`

	dailyJSON = `{
    "city": {
        "coord": {
            "lat": 34.966671,
            "lon": 138.933334
        },
        "country": "JP",
        "id": 1851632,
        "name": "Shuzenji",
        "population": 0
    },
    "cnt": 10,
    "cod": "200",
    "list": [
        {
            "clouds": 92,
            "deg": 309,
            "dt": 1450836000,
            "humidity": 100,
            "pressure": 990.01,
            "rain": 2.37,
            "speed": 0.97,
            "temp": {
                "day": 280.01,
                "eve": 280.02,
                "max": 280.03,
                "min": 280.04,
                "morn": 280.05,
                "night": 280.06
            },
            "weather": [
                {
                    "description": "light rain",
                    "icon": "10d",
                    "id": 500,
                    "main": "Rain"
                }
            ],
						"speed":3.36,
						"deg":199,
						"clouds":80
        }
    ],
    "message": 0.0289
}`
)

func TestWeatherInfoString(t *testing.T) {
	// Some values
	main, desc := "Main", "Desc"

	// Create a WeatherInfo
	weather := WeatherInfo{Type: main, Description: desc}

	// Create the expected
	expected := main + " " + desc

	// Check the string
	if actual := weather.String(); actual != expected {
		t.Errorf("Expected weather info string to be \"%s\" but was \"%s\"",
			expected, actual)
	} //if
} //TestWeatherInfoString

func TestUnmarshalHourly(t *testing.T) {
	outlook, err := unmarshalHourly([]byte(hourlyJSON))

	// Check for an error
	if err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	// Check for nil outlook
	if outlook == nil {
		t.Error("Did not expect the outlook to be nil")
		return
	} //if

	// Check the city coord's latitude
	if expected, actual := 34.966671, outlook.City.Coord.Lat; expected != actual {
		t.Error("Expected city latitude to be", expected, "but was", actual)
	} //if

	// Check the city coord's longitude
	if expected, actual := 138.933334, outlook.City.Coord.Lon; expected != actual {
		t.Error("Expected city longitude to be", expected, "but was", actual)
	} //if

	// Check the city country
	if expected, actual := "JP", outlook.City.Country; expected != actual {
		t.Errorf("Expected city country to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the city id
	if expected, actual := 1851632, outlook.City.ID; expected != actual {
		t.Error("Expected city id to be", expected, "but was", actual)
	} //if

	// Check the city name
	if expected, actual := "Shuzenji", outlook.City.Name; expected != actual {
		t.Errorf("Expected city name to be \"%s\" but was \"%s\"", expected, actual)
	} //if

	// Make sure there is one forecast
	if numForecast := len(outlook.Forecasts); numForecast != 1 {
		t.Error("Expected 1 forecast but there were", numForecast)
		return
	} //if

	// Get the forecast
	forecast := outlook.Forecasts[0]

	// Check for time stamp
	if expected, actual := int64(1450915200), forecast.UnixStamp; expected != actual {
		t.Error("Expected forecast timestamp to be", expected, "but was", actual)
	} //if

	// Check the time string
	if expected, actual := "2015-12-24 00:00:00", forecast.TimeString; expected != actual {
		t.Error("Expected forecast time string to be", expected, "but was", actual)
	} //if

	// Check the humidity
	if expected, actual := 96, forecast.Main.Humidity; expected != actual {
		t.Error("Expected forecast humidity to be", expected, "but was", actual)
	} //if

	// Check the pressure
	if expected, actual := float32(930.3), forecast.Main.Pressure; expected != actual {
		t.Error("Expected forecast pressure to be", expected, "but was", actual)
	} //if

	// Check the temperature
	if expected, actual := Temperature(281.64), forecast.Main.Temp; expected != actual {
		t.Error("Expected forecast temperature to be", expected, "but was", actual)
	} //if

	// Check the weather id
	if expected, actual := 500, forecast.Weather[0].ID; expected != actual {
		t.Error("Expected weather id to be", expected, "but was", actual)
	} //if

	// Check the weather type
	if expected, actual := "Rain", forecast.Weather[0].Type; expected != actual {
		t.Errorf("Expected weather type to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the weather description
	if expected, actual := "light rain", forecast.Weather[0].Description; expected != actual {
		t.Errorf("Expected weather type to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the weather icon
	if expected, actual := "10d", forecast.Weather[0].Icon; expected != actual {
		t.Errorf("Expected weather icon to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the wind speed
	if expected, actual := float32(0.97), forecast.Wind.Speed; expected != actual {
		t.Error("Expected wind speed to be", expected, "but was", actual)
	} //if

	// Check the wind direction
	if expected, actual := float32(338.502), forecast.Wind.Direction; expected != actual {
		t.Error("Expected wind direction to be", expected, "but was", actual)
	} //if

	// Check the clouds
	if expected, actual := 8, forecast.Clouds.All; expected != actual {
		t.Error("Expected clouds percentage to be", expected, "but was", actual)
	} //if
} //TestUnmarshalHourly

func TestUnmarshalDaily(t *testing.T) {
	outlook, err := unmarshalDaily([]byte(dailyJSON))

	// Check for an error
	if err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	// Check for nil outlook
	if outlook == nil {
		t.Error("Did not expect the outlook to be nil")
		return
	} //if

	// Check the city coord's latitude
	if expected, actual := 34.966671, outlook.City.Coord.Lat; expected != actual {
		t.Error("Expected city latitude to be", expected, "but was", actual)
	} //if

	// Check the city coord's longitude
	if expected, actual := 138.933334, outlook.City.Coord.Lon; expected != actual {
		t.Error("Expected city longitude to be", expected, "but was", actual)
	} //if

	// Check the city country
	if expected, actual := "JP", outlook.City.Country; expected != actual {
		t.Errorf("Expected city country to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the city id
	if expected, actual := 1851632, outlook.City.ID; expected != actual {
		t.Error("Expected city id to be", expected, "but was", actual)
	} //if

	// Check the city name
	if expected, actual := "Shuzenji", outlook.City.Name; expected != actual {
		t.Errorf("Expected city name to be \"%s\" but was \"%s\"", expected, actual)
	} //if

	// Make sure there is one forecast
	if numForecast := len(outlook.Forecasts); numForecast != 1 {
		t.Error("Expected 1 forecast but there were", numForecast)
		return
	} //if

	// Get the forecast
	forecast := outlook.Forecasts[0]

	// Check for time stamp
	if expected, actual := int64(1450836000), forecast.UnixStamp; expected != actual {
		t.Error("Expected forecast timestamp to be", expected, "but was", actual)
	} //if

	// Check the humidity
	if expected, actual := 100, forecast.Humidity; expected != actual {
		t.Error("Expected forecast humidity to be", expected, "but was", actual)
	} //if

	// Check the pressure
	if expected, actual := float32(990.01), forecast.Pressure; expected != actual {
		t.Error("Expected forecast pressure to be", expected, "but was", actual)
	} //if

	// Check the rain
	if expected, actual := float32(2.37), forecast.Rain; expected != actual {
		t.Error("Expected forecast rain to be", expected, "but was", actual)
	} //if

	// Check the weather type
	if expected, actual := "Rain", forecast.Weather[0].Type; expected != actual {
		t.Errorf("Expected weather type to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the weather description
	if expected, actual := "light rain", forecast.Weather[0].Description; expected != actual {
		t.Errorf("Expected weather type to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the day temperature
	if expected, actual := Temperature(280.01), forecast.Temp.Day; expected != actual {
		t.Error("Expected day temperature to be", expected, "but was", actual)
	} //if

	// Check the evening temperature
	if expected, actual := Temperature(280.02), forecast.Temp.Evening; expected != actual {
		t.Error("Expected evening temperature to be", expected, "but was", actual)
	} //if

	// Check the max temperature
	if expected, actual := Temperature(280.03), forecast.Temp.Max; expected != actual {
		t.Error("Expected max temperature to be", expected, "but was", actual)
	} //if

	// Check the min temperature
	if expected, actual := Temperature(280.04), forecast.Temp.Min; expected != actual {
		t.Error("Expected min temperature to be", expected, "but was", actual)
	} //if

	// Check the morning temperature
	if expected, actual := Temperature(280.05), forecast.Temp.Morning; expected != actual {
		t.Error("Expected morning temperature to be", expected, "but was", actual)
	} //if

	// Check the night temperature
	if expected, actual := Temperature(280.06), forecast.Temp.Night; expected != actual {
		t.Error("Expected night temperature to be", expected, "but was", actual)
	} //if

	// Check the weather id
	if expected, actual := 500, forecast.Weather[0].ID; expected != actual {
		t.Error("Expected weather id to be", expected, "but was", actual)
	} //if

	// Check the weather type
	if expected, actual := "Rain", forecast.Weather[0].Type; expected != actual {
		t.Errorf("Expected weather type to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the weather description
	if expected, actual := "light rain", forecast.Weather[0].Description; expected != actual {
		t.Errorf("Expected weather type to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the weather icon
	if expected, actual := "10d", forecast.Weather[0].Icon; expected != actual {
		t.Errorf("Expected weather icon to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if

	// Check the wind speed
	if expected, actual := float32(3.36), forecast.WindSpeed; expected != actual {
		t.Error("Expected wind speed to be", expected, "but was", actual)
	} //if

	// Check the wind direction
	if expected, actual := 199, forecast.WindDirection; expected != actual {
		t.Error("Expected wind direction to be", expected, "but was", actual)
	} //if

	// Check the cloud percentage
	if expected, actual := 80, forecast.Clouds; expected != actual {
		t.Error("Expected cloud percentage to be", expected, "but was", actual)
	} //if
} //TestUnmarshalDaily
