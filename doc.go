// Package openweathermap provides convient mechanisms for accessing
// openweathermap.org APIs.
//
// In order to use this package one must signup and get
// an API key (view the README file for a link to the signup page). Using the
// APICaller one can request for hourly and daily forecasts. NOTE: The hourly
// forecasts are actually in 3 hours intervals and the daily goes out to a
// maximum of sixteen days.
//
// This package is licensed under the terms of the MIT license. See LICENSE
// file for more details.
package openweathermap
