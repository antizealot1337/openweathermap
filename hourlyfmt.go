package openweathermap

import (
	"fmt"
	"strings"
)

// hourlyFmt is the format for getting the hourly forecast.
type hourlyFmt string

const (
	// HourlyLocFmt is the format to request the hourly forecast using the
	// location.
	hourlyLocFmt hourlyFmt = "http://api.openweathermap.org/data/2.5/forecast?lat=%f&lon=%f&appid=%s"

	// HourlyCityFmt is the format to request the hourly forecast using the city
	// and country code.
	hourlyCityFmt hourlyFmt = "http://api.openweathermap.org/data/2.5/forecast?q=\"%s,%s\"&appid=%s"
)

// FormatLoc formats the HourlyFmt to get the hourly forecast using the latitude
// and longitude.
func (h hourlyFmt) FormatLoc(latitude, longitude float64, key string) string {
	return fmt.Sprintf(string(h), latitude, longitude, key)
} //FormatLoc

// FormatCity formats the HourlyFmt to get the hourly forecast using the cite
// and country code provided.
func (h hourlyFmt) FormatCity(city, country, key string) string {
	return fmt.Sprintf(string(h), strings.Replace(city, " ", "+", -1), country,
		key)
} //FormatCity
