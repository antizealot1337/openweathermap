package openweathermap

import "testing"

func TestNewAPICaller(t *testing.T) {
	// Some values
	key := "abc"

	// Create an APICaller
	caller := NewAPICaller(key)

	// Check the key
	if caller.key != key {
		t.Errorf("Expected key to be \"%s\" but was \"%s\"", key, caller.key)
	} //if
} //TestNewAPICaller
