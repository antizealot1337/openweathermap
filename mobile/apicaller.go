package mobile

import "github.com/antizealot1337/openweathermap"

func fromRequest(city openweathermap.City) *CityInfo {
	return &CityInfo{
		Name:    city.Name,
		Country: city.Country,
		Lat:     city.Coord.Lat,
		Long:    city.Coord.Lon,
	}
} //fromRequest

// APICaller is used make requests to OpenWeatherMap.org to get forecasts.
type APICaller struct {
	caller *openweathermap.APICaller
} //struct APICaller

// NewAPICaller creates a new APICaller.
func NewAPICaller(key string) *APICaller {
	// Create a new APICaller
	return &APICaller{
		caller: openweathermap.NewAPICaller(key),
	}
} //NewAPICaller

// HourlyByLoc gets the hourly forecasts by location.
func (a *APICaller) HourlyByLoc(latitude, longitude float64) (*Outlook, error) {
	// Get the hourly forecast
	outlook, err := a.caller.HourlyByLoc(latitude, longitude)

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	return fromHourlyOutlook(outlook), nil
} //HourlyByLoc

// HourlyByCity gets the hourly forecasts by city and country.
func (a *APICaller) HourlyByCity(city, country string) (*Outlook, error) {
	// Get the hourly forecast
	outlook, err := a.caller.HourlyByCity(city, country)

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	return fromHourlyOutlook(outlook), nil
} //HourlyByCity

// DailyByLoc gets the daily forecasts by the location.
func (a *APICaller) DailyByLoc(latitude, longitude float64, days int) (*Outlook, error) {
	// Get the forecast
	outlook, err := a.caller.DailyByLoc(latitude, longitude, days)

	// Check from an error
	if err != nil {
		return nil, err
	} //if

	return fromDailyOutlook(outlook), nil
} //DailyByLoc

// DailyByCity gets the daily forecasts by the city and country.
func (a *APICaller) DailyByCity(city, country string, days int) (*Outlook, error) {
	// Get the forecast
	outlook, err := a.caller.DailyByCity(city, country, days)

	// Check from an error
	if err != nil {
		return nil, err
	} //if

	return fromDailyOutlook(outlook), nil
} //DailyByCity
