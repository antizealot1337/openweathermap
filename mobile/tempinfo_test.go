package mobile

import (
	"testing"

	"github.com/antizealot1337/openweathermap"
)

func TestTempInfoTestValue(t *testing.T) {
	// Some values
	var temp openweathermap.Temperature = 300

	// Create the TempInfo
	tinfo := &TempInfo{value: temp}

	// Check for an error
	if _, err := tinfo.Value(-1); err == nil {
		t.Error("Expected error for bad value(-1)")
	} else if _, err := tinfo.Value(DegressFahrenheit + 1); err == nil {
		t.Error("Expected error for bad value(3)")
	} //if

	// Get the Kelvin temperature
	expected := temp.ToK()

	// Check the Kelvin value
	if actual, err := tinfo.Value(DegreesKelvin); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Kelvin value to be", expected, "but was", actual)
	} //if

	// Get the Celcius temperature
	expected = temp.ToC()

	// Check the Celcius value
	if actual, err := tinfo.Value(DegreesCelcius); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Celcius value to be", expected, "but was", actual)
	} //if

	// Get the Fahrenheit temperature
	expected = temp.ToF()

	// Check the Fahrenheit value
	if actual, err := tinfo.Value(DegressFahrenheit); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Fahrenheit value to be", expected, "but was", actual)
	} //if
} //TestTempInfoTestValue

func TestTempInfoTestMax(t *testing.T) {
	// Some values
	var temp openweathermap.Temperature = 300

	// Create the TempInfo
	tinfo := &TempInfo{max: temp}

	// Check for an error
	if _, err := tinfo.Max(-1); err == nil {
		t.Error("Expected error for bad value(-1)")
	} else if _, err := tinfo.Max(DegressFahrenheit + 1); err == nil {
		t.Error("Expected error for bad value(3)")
	} //if

	// Get the Kelvin temperature
	expected := temp.ToK()

	// Check the Kelvin value
	if actual, err := tinfo.Max(DegreesKelvin); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Kelvin value to be", expected, "but was", actual)
	} //if

	// Get the Celcius temperature
	expected = temp.ToC()

	// Check the Celcius value
	if actual, err := tinfo.Max(DegreesCelcius); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Celcius value to be", expected, "but was", actual)
	} //if

	// Get the Fahrenheit temperature
	expected = temp.ToF()

	// Check the Fahrenheit value
	if actual, err := tinfo.Max(DegressFahrenheit); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Fahrenheit value to be", expected, "but was", actual)
	} //if
} //TestTempInfoTestMax

func TestTempInfoTestMin(t *testing.T) {
	// Some values
	var temp openweathermap.Temperature = 300

	// Create the TempInfo
	tinfo := &TempInfo{min: temp}

	// Check for an error
	if _, err := tinfo.Min(-1); err == nil {
		t.Error("Expected error for bad valud(-1)")
	} else if _, err := tinfo.Min(DegressFahrenheit + 1); err == nil {
		t.Error("Expected error for bad valud(3)")
	} //if

	// Get the Kelvin temperature
	expected := temp.ToK()

	// Check the Kelvin value
	if actual, err := tinfo.Min(DegreesKelvin); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Kelvin value to be", expected, "but was", actual)
	} //if

	// Get the Celcius temperature
	expected = temp.ToC()

	// Check the Celcius value
	if actual, err := tinfo.Min(DegreesCelcius); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Celcius value to be", expected, "but was", actual)
	} //if

	// Get the Fahrenheit temperature
	expected = temp.ToF()

	// Check the Fahrenheit value
	if actual, err := tinfo.Min(DegressFahrenheit); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Fahrenheit value to be", expected, "but was", actual)
	} //if
} //TestTempInfoTestMin

func TestTempInfoTestDay(t *testing.T) {
	// Some values
	var temp openweathermap.Temperature = 300

	// Create the TempInfo
	tinfo := &TempInfo{day: temp}

	// Check for an error
	if _, err := tinfo.Day(-1); err == nil {
		t.Error("Expected error for bad valud(-1)")
	} else if _, err := tinfo.Day(DegressFahrenheit + 1); err == nil {
		t.Error("Expected error for bad valud(3)")
	} //if

	// Get the Kelvin temperature
	expected := temp.ToK()

	// Check the Kelvin value
	if actual, err := tinfo.Day(DegreesKelvin); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Kelvin value to be", expected, "but was", actual)
	} //if

	// Get the Celcius temperature
	expected = temp.ToC()

	// Check the Celcius value
	if actual, err := tinfo.Day(DegreesCelcius); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Celcius value to be", expected, "but was", actual)
	} //if

	// Get the Fahrenheit temperature
	expected = temp.ToF()

	// Check the Fahrenheit value
	if actual, err := tinfo.Day(DegressFahrenheit); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Fahrenheit value to be", expected, "but was", actual)
	} //if
} //TestTempInfoTestDay

func TestTempInfoTestNight(t *testing.T) {
	// Some values
	var temp openweathermap.Temperature = 300

	// Create the TempInfo
	tinfo := &TempInfo{night: temp}

	// Check for an error
	if _, err := tinfo.Night(-1); err == nil {
		t.Error("Expected error for bad valud(-1)")
	} else if _, err := tinfo.Night(DegressFahrenheit + 1); err == nil {
		t.Error("Expected error for bad valud(3)")
	} //if

	// Get the Kelvin temperature
	expected := temp.ToK()

	// Check the Kelvin value
	if actual, err := tinfo.Night(DegreesKelvin); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Kelvin value to be", expected, "but was", actual)
	} //if

	// Get the Celcius temperature
	expected = temp.ToC()

	// Check the Celcius value
	if actual, err := tinfo.Night(DegreesCelcius); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Celcius value to be", expected, "but was", actual)
	} //if

	// Get the Fahrenheit temperature
	expected = temp.ToF()

	// Check the Fahrenheit value
	if actual, err := tinfo.Night(DegressFahrenheit); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual != expected {
		t.Error("Expected Fahrenheit value to be", expected, "but was", actual)
	} //if
} //TestTempInfoTestNight
