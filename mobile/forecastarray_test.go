package mobile

import "testing"

func TestNewForecastArray(t *testing.T) {
	// Create a ForecastArray
	farray := NewForecastArray()

	// Make sure the array isn't nil
	if farray == nil {
		t.Error("Expected the ForecastArray to not be nil")
	} //if

	// Make sure the forecasts is initialized
	if farray.forecasts == nil {
		t.Error("Expected the slice of the ForecastArray to be initialized")
	} //if
} //TestNewForecastArray

func TestForecastArrayLen(t *testing.T) {
	// Create a ForecastArray
	farray := ForecastArray{}

	// Check the length
	if expected, actual := len(farray.forecasts), farray.Len(); expected != actual {
		t.Error("Expected the length to be", expected, "but was", actual)
	} //if

	// Add a Forecast
	farray.forecasts = append(farray.forecasts, &Forecast{})

	// Check the length
	if expected, actual := len(farray.forecasts), farray.Len(); expected != actual {
		t.Error("Expected the length to be", expected, "but was", actual)
	} //if
} //TestForecastArrayLen

func TestForecastArrayGet(t *testing.T) {
	// Create some Forecasts
	f1, f2 := &Forecast{}, &Forecast{}

	// Create a ForecastArray
	farray := ForecastArray{
		forecasts: []*Forecast{f1, f2},
	}

	// Check for an error
	if _, err := farray.Get(-1); err == nil {
		t.Error("Expected error for bad index(-1)")
	} else if _, err := farray.Get(2); err == nil {
		t.Error("Expected error for bad index(2)")
	} //if

	// Check getting the Forecasts
	if f, err := farray.Get(0); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if f != f1 {
		t.Error("Expected the index to yield the first forecast")
	} //if

	// Check getting the Forecasts
	if f, err := farray.Get(1); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if f != f2 {
		t.Error("Expected the index to yield the second forecast")
	} //if
} //TestForecastArrayGet

func TestForecastArrayAdd(t *testing.T) {
	// Create a ForecastArray
	farray := ForecastArray{}

	// Create a Forecast
	f := &Forecast{}

	// Add to the ForecastArray
	farray.Add(f)

	// Check the length
	if expected, actual := 1, len(farray.forecasts); expected != actual {
		t.Error("Expected the length after adding to be", expected, "but was",
			actual)
	} //if

	// Check to make sure the item was added
	if actual := farray.forecasts[0]; actual != f {
		t.Error("Expected the forecast to be in the slice")
	} //if
} //TestForecastArrayAdd
