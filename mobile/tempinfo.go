package mobile

import (
	"errors"

	"github.com/antizealot1337/openweathermap"
)

// Different temperature messurement scales.
const (
	DegreesKelvin     int32 = iota
	DegreesCelcius    int32 = iota
	DegressFahrenheit int32 = iota
)

var errBadTempMessure = errors.New("Invalid temperature messurement type")

// TempInfo holds temperature values
type TempInfo struct {
	value openweathermap.Temperature
	min   openweathermap.Temperature
	max   openweathermap.Temperature
	day   openweathermap.Temperature
	night openweathermap.Temperature
} //struct TempInfo

// Value of the TempInfo.
func (t *TempInfo) Value(messureIn int32) (float32, error) {
	// Determine how to return the temperature
	switch messureIn {
	case DegreesKelvin:
		return t.value.ToK(), nil
	case DegreesCelcius:
		return t.value.ToC(), nil
	case DegressFahrenheit:
		return t.value.ToF(), nil
	} //switch

	return 0, errBadTempMessure
} //Value

// Min temperature.
func (t *TempInfo) Min(messureIn int32) (float32, error) {
	// Determine how to return the temperature
	switch messureIn {
	case DegreesKelvin:
		return t.min.ToK(), nil
	case DegreesCelcius:
		return t.min.ToC(), nil
	case DegressFahrenheit:
		return t.min.ToF(), nil
	} //switch

	return 0, errBadTempMessure
} //Min

// Max temperature.
func (t *TempInfo) Max(messureIn int32) (float32, error) {
	// Determine how to return the temperature
	switch messureIn {
	case DegreesKelvin:
		return t.max.ToK(), nil
	case DegreesCelcius:
		return t.max.ToC(), nil
	case DegressFahrenheit:
		return t.max.ToF(), nil
	} //switch

	return 0, errBadTempMessure
} //Max

// Day time temperature.
func (t *TempInfo) Day(messureIn int32) (float32, error) {
	// Determine how to return the temperature
	switch messureIn {
	case DegreesKelvin:
		return t.day.ToK(), nil
	case DegreesCelcius:
		return t.day.ToC(), nil
	case DegressFahrenheit:
		return t.day.ToF(), nil
	} //switch

	return 0, errBadTempMessure
} //Day

// Night time temperature.
func (t *TempInfo) Night(messureIn int32) (float32, error) {
	// Determine how to return the temperature
	switch messureIn {
	case DegreesKelvin:
		return t.night.ToK(), nil
	case DegreesCelcius:
		return t.night.ToC(), nil
	case DegressFahrenheit:
		return t.night.ToF(), nil
	} //switch

	return 0, errBadTempMessure
} //Night
