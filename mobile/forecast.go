package mobile

import (
	"time"

	"github.com/antizealot1337/openweathermap"
)

// Forecast data
type Forecast struct {
	timestamp   int64
	Humidity    int32
	Pressure    float32
	Weather     string
	WeatherDesc string
	Temp        *TempInfo
} //struct Forecast

func hourlyForecast(forecast *openweathermap.HourlyForecast) *Forecast {
	return &Forecast{
		timestamp:   forecast.UnixStamp,
		Humidity:    int32(forecast.Main.Humidity),
		Pressure:    forecast.Main.Pressure,
		Weather:     forecast.Weather[0].Type,
		WeatherDesc: forecast.Weather[0].Description,
	}
} //hourlyForecast

func dailyForecast(forecast *openweathermap.DailyForecast) *Forecast {
	return &Forecast{
		timestamp:   forecast.UnixStamp,
		Humidity:    int32(forecast.Humidity),
		Pressure:    forecast.Pressure,
		Weather:     forecast.Weather[0].Type,
		WeatherDesc: forecast.Weather[0].Description,
	}
} //dailyForecast

// UnixStamp is a unix epoch based time stamp.
func (f *Forecast) UnixStamp() int64 {
	return f.timestamp
} //UnixStamp

// TimeString represents the time value as a string.
func (f *Forecast) TimeString() string {
	return time.Unix(f.timestamp, 0).String()
} //TimeString
