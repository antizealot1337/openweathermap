package mobile

import "github.com/antizealot1337/openweathermap"

// CityInfo from the forecast.
type CityInfo struct {
	Name    string
	Country string
	Lat     float64
	Long    float64
} //struct City

// Outlook contains the hourly forecast from OpenWeatherMap.org
type Outlook struct {
	City      *CityInfo
	Forecasts *ForecastArray
} //struct Data

func fromHourlyOutlook(outlook *openweathermap.HourlyOutlook) *Outlook {
	// Create the ForecastArray
	farray := NewForecastArray()

	// Loop through the DailyForecasts
	for _, forecast := range outlook.Forecasts {
		farray.Add(hourlyForecast(&forecast))
	} //for

	return &Outlook{
		City:      fromRequest(outlook.City),
		Forecasts: farray,
	}
} //fromHourlyOutlook

func fromDailyOutlook(outlook *openweathermap.DailyOutlook) *Outlook {
	// Create the ForecastArray
	farray := NewForecastArray()

	// Loop through the DailyForecasts
	for _, forecast := range outlook.Forecasts {
		farray.Add(dailyForecast(&forecast))
	} //for

	return &Outlook{
		City:      fromRequest(outlook.City),
		Forecasts: farray,
	}
} //fromDailyOutlook
