package mobile

import "errors"

var errInvalidIndex = errors.New("forecast array error: invalid index")

// ForecastArray is an array of Forecast data.
type ForecastArray struct {
	forecasts []*Forecast
} //struct ForecastArray

// NewForecastArray creates a new ForecastArray.
func NewForecastArray() *ForecastArray {
	return &ForecastArray{
		forecasts: make([]*Forecast, 0),
	}
} //NewForecastArray

// Len is the length of the array.
func (f *ForecastArray) Len() int {
	return len(f.forecasts)
} //Len

// Get returns a Forecast at the supplied index. If the index is out of bounds
// and error is returned.
func (f *ForecastArray) Get(index int) (*Forecast, error) {
	// Check if the index is less than zero
	if index < 0 {
		return nil, errInvalidIndex
	} //if

	// Check if the index is greater than the length of the array
	if index >= len(f.forecasts) {
		return nil, errInvalidIndex
	} //if

	return f.forecasts[index], nil
} //Get

// Add adds a Forecast to the ForecastArray.
func (f *ForecastArray) Add(forecast *Forecast) {
	f.forecasts = append(f.forecasts, forecast)
} //Add
