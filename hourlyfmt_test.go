package openweathermap

import "testing"

func TestHourlyFmtFormatLoc(t *testing.T) {
	// Some values
	lat, lon, key, fmtStr := 1.0, 2.0, "a", hourlyFmt("%f %f %s")

	// The expected string
	expected := "1.000000 2.000000 a"

	// Check the location format
	if actual := fmtStr.FormatLoc(lat, lon, key); actual != expected {
		t.Errorf("Expected hourly format to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if
} //TestHourlyFmtFormatLoc

func TestHourlyFmtFormatCity(t *testing.T) {
	// Some values
	city, country, key, fmtStr := "City", "US", "a", hourlyFmt("%s %s %s")

	// The expected string
	expected := "City US a"

	// Check the city format
	if actual := fmtStr.FormatCity(city, country, key); actual != expected {
		t.Errorf("Expected hourly format to be \"%s\" but was \"%s\"", expected,
			actual)
	} //if
} //TestHourlyFmtFormatCity
