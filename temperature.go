package openweathermap

// Temperature is a floating point number that represents the a Kelvin
// temperature.
type Temperature float32

// ToK returns the Kelvin temperature as a float32 value.
func (t Temperature) ToK() float32 {
	return float32(t)
}

// ToC returns the calculated Celcius temperature as a float32 value.
func (t Temperature) ToC() float32 {
	// Cast to a float
	temp := float64(t)
	return float32(temp - 273.15)
} //ToC

// ToF returns the calculated Fahrenheit temperature as a float32 value.
func (t Temperature) ToF() float32 {
	// Cast to a float
	temp := float64(t)

	// Do the calculations
	return float32(temp*9.0/5.0 - 459.67)
} //ToF
