package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/antizealot1337/openweathermap"
)

const (
	verStr    = "Forecast v0.1 powered by openweathermap.org"
	copyright = `Copyright (c) 2015 Christopher Pridgen


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.`
)

func main() {
	lat := flag.Float64("lat", 0, "The latitude")
	lon := flag.Float64("lon", 0, "The longitude")
	city := flag.String("city", "",
		"The city. When used with country lat and lon are ignored")
	country := flag.String("country", "",
		"The ISO 3166 country code. When used with city lat an lon are ignored.")
	days := flag.Int("days", 16, "The number of days for the forecast")
	hourly := flag.Bool("hourly", false,
		"Get an hourly forecast instead of a daily forecast. (Forces days to be ingored)")
	degree := flag.String("degree", "C",
		"The type of temperature messurement to display. Possibles are K, C, and F")
	version := flag.Bool("version", false, "Prints the version then exits.")
	license := flag.Bool("license", false, "Prints the license then exits.")

	flag.Parse()

	// Check if we are just printing the version and exiting.
	if *version {
		fmt.Println(verStr)
		os.Exit(0)
	} //if

	// Check if we are just printint the license and exiting.
	if *license {
		fmt.Println(copyright)
		os.Exit(0)
	} //if

	// Validate the temperature measurement type
	switch *degree {
	case "K", "k", "C", "c", "F", "f":
	default:
		fmt.Printf("%s is not a valid temperature meassurement type\n", *degree)
		os.Exit(-1)
	} //switch

	// Perform a network connection check
	if conn, err := net.Dial("tcp", "google.com:80"); err != nil {
		fmt.Println("Error: No network connection")
		os.Exit(-1)
	} else {
		// Close the connection
		conn.Close()
	} //if

	// Determine if the location should be used or the city/country
	useLocation := *city == "" && *country == ""

	// Create an APICaller
	caller := openweathermap.NewAPICaller(openweathermap.APIKey)

	if *hourly {
		// The values for the hourly outlook request
		var outlook *openweathermap.HourlyOutlook
		var err error

		// Check if we are using the location or city+country
		if useLocation {
			outlook, err = caller.HourlyByLoc(*lat, *lon)
		} else {
			outlook, err = caller.HourlyByCity(*city, *country)
		} //if

		// Check for the error
		if err != nil {
			fmt.Println("Unable to get forecast")
			os.Exit(-1)
		} //if

		// Show the hourly forecast
		showHourly(outlook, *degree)
	} else {
		// The value for the daily outlook request
		var outlook *openweathermap.DailyOutlook
		var err error

		// Check if we are using the location or city+country
		if useLocation {
			outlook, err = caller.DailyByLoc(*lat, *lon, *days)
		} else {
			outlook, err = caller.DailyByCity(*city, *country, *days)
		} //if

		// Check for the error
		if err != nil {
			fmt.Println("Unable to get forecast")
			os.Exit(-1)
		} //if

		// Show the daily forecast
		showDaily(outlook, *degree)
	} //if
} //main

func showDaily(outlook *openweathermap.DailyOutlook, degree string) {
	fmt.Printf("Daily Forecast for %s %s\n", outlook.City.Name,
		outlook.City.Country)

	// Loop through the forecasts
	for _, forecast := range outlook.Forecasts {
		var day float32
		var night float32

		switch degree {
		case "K", "k":
			day, night = forecast.Temp.Day.ToK(), forecast.Temp.Night.ToK()
		case "C", "c":
			day, night = forecast.Temp.Day.ToC(), forecast.Temp.Night.ToC()
		case "F", "f":
			day, night = forecast.Temp.Day.ToF(), forecast.Temp.Night.ToF()
		} //switch

		// Create a Time from the time stamp
		ts := time.Unix(int64(forecast.UnixStamp), 0)

		fmt.Printf("%s %-25s Day %5.2f Night %5.2f Humidity %3d Pressure %.3f\n",
			ts.String(), forecast.Weather[0].String(), day, night, forecast.Humidity,
			forecast.Pressure)
	} //for
} //showDaily

func showHourly(outlook *openweathermap.HourlyOutlook, degree string) {
	fmt.Printf("Hourly Forecast for %s %s\n", outlook.City.Name,
		outlook.City.Country)

	// Loop through the forecasts
	for _, forecast := range outlook.Forecasts {
		var temp float32

		switch degree {
		case "K", "k":
			temp = forecast.Main.Temp.ToK()
		case "C", "c":
			temp = forecast.Main.Temp.ToC()
		case "F", "f":
			temp = forecast.Main.Temp.ToF()
		} //switch

		// Create a Time from the time stamp
		ts := time.Unix(forecast.UnixStamp, 0)

		fmt.Printf("%s %-25s Temperature %5.2f Humidity %3d Pressure %.3f\n",
			ts.String(), forecast.Weather[0].String(), temp,
			forecast.Main.Humidity, forecast.Main.Pressure)
	} //for
} //showHourly
