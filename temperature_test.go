package openweathermap

import "testing"

func TestTemperatureToK(t *testing.T) {
	// Some values
	var k float32 = 30

	// Create a temperature
	temp := Temperature(k)

	// Check the kelvin float value
	if actual := temp.ToK(); actual != k {
		t.
			Error("Expected kelvin temperature to be", k, "but was", actual)
	} //if
} //TestTemperatureToK

func TestTemperatureToC(t *testing.T) {
	// Some values
	var k float32 = 300
	var c float32 = 26.85

	// Create a temperature
	temp := Temperature(k)

	// Check the kelvin float value
	if actual := temp.ToC(); actual != c {
		t.
			Error("Expected celcius temperature to be", c, "but was", actual)
	} //if
} //TestTemperatureToC

func TestTemperatureToF(t *testing.T) {
	// Some values
	var k float32 = 30
	var f float32 = -405.67

	// Create a temperature
	temp := Temperature(k)

	// Check the kelvin float value
	if actual := temp.ToF(); actual != f {
		t.
			Error("Expected fahrenheit temperature to be", f, "but was", actual)
	} //if
} //TestTemperatureToF
