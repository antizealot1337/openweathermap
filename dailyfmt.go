package openweathermap

import (
	"fmt"
	"strings"
)

// DailyFmt is a format string for a request for a daily forecast.
type dailyFmt string

const (
	// DailyLocFmt is the daily forecast api string using the location.
	dailyLocFmt dailyFmt = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=%f&lon=%f&cnt=%d&appid=%s"

	// DailyCityFmt is the daily forecase api string using the city name and
	// country code.
	dailyCityFmt dailyFmt = "http://api.openweathermap.org/data/2.5/forecast/daily?q=\"%s,%s\"&cnt=%d&appid=%s"
)

// FormatLoc formats the DailyFmt using the location latitude and longitude to
// get the forecast for the specified number of days.
func (d dailyFmt) FormatLoc(latitude, longitude float64, day int, key string) string {
	return fmt.Sprintf(string(d), latitude, longitude, day, key)
} //FormatLoc

// FormatCity formats the DailyFmt using the location name to get the forecast
// for the specified number of days.
func (d dailyFmt) FormatCity(city, country string, day int, key string) string {
	return fmt.Sprintf(string(d), strings.Replace(city, " ", "+", -1), country,
		day, key)
} //FormatName
