package openweathermap

import "testing"

func TestDailyFmtFormatLoc(t *testing.T) {
	// Some values
	lat, lon, days, key := 1.0, 2.0, 3, "4"

	// The DailyFmt string
	var test dailyFmt = "%f %f %d %s"

	// The expected string
	expected := "1.000000 2.000000 3 4"

	// Check the format
	if actual := test.FormatLoc(lat, lon, days, key); expected != actual {
		t.Errorf("Expected formatted string to be \"%s\" but was \"%s\"",
			expected, actual)
	} //if
} //TestDailyFmtFormatLoc

func TestDailyFmtFormatCity(t *testing.T) {
	// Some values
	name, country, days, key := "Name", "US", 3, "4"

	// The DailyFmt string
	var test dailyFmt = "%s %s %d %s"

	// The expected string
	expected := "Name US 3 4"

	// Check the format
	if actual := test.FormatCity(name, country, days, key); expected != actual {
		t.Errorf("Expected formatted string to be \"%s\" but was \"%s\"",
			expected, actual)
	} //if
} //TestDailyFmtFormatCity
