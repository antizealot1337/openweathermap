package openweathermap

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
)

// APICaller calls the OpenWeatherMap.org APIs
type APICaller struct {
	key string
} //struct APICaller

// NewAPICaller creates and and returns a new APICaller
func NewAPICaller(key string) *APICaller {
	return &APICaller{
		key: key,
	}
} //NewAPICaller

// HourlyByCity gets the hourly Forecast by the city and country name.
func (a *APICaller) HourlyByCity(name, country string) (*HourlyOutlook, error) {
	// Build and make the request
	data, err := makeRequest(hourlyCityFmt.FormatCity(name, country, a.key))

	// Check for the error
	if err != nil {
		return nil, err
	} //if

	// Return the unmarshalled data
	return unmarshalHourly(data)
} //HourlyByCity

// DailyByCity get the daily Forecast for the supplied number of days by the
// city and country name.
func (a *APICaller) DailyByCity(name, country string, days int) (*DailyOutlook, error) {
	// Check the days
	if days < 1 || days > 16 {
		return nil, errors.New("day error: invalid day value")
	} //if

	// Build and make the request
	data, err := makeRequest(dailyCityFmt.FormatCity(name, country, days,
		a.key))

	// Check for the error
	if err != nil {
		return nil, err
	} //if

	// Retrun the unmarshalled data
	return unmarshalDaily(data)
} //DailyByCity

// HourlyByLoc get the hourly Forecast by the latitude and longitude.
func (a *APICaller) HourlyByLoc(latitude, longitude float64) (*HourlyOutlook, error) {
	// Build and make the request
	data, err := makeRequest(hourlyLocFmt.FormatLoc(latitude, longitude, a.key))

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	// Return the unmarshalled data
	return unmarshalHourly(data)
} //HourlyByLoc

// DailyByLoc get the daily Forecast for the supplied number of days by the
// latitude and longitude.
func (a *APICaller) DailyByLoc(latitude, longitude float64, days int) (*DailyOutlook, error) {
	// Check the days
	if days < 1 || days > 16 {
		return nil, errors.New("day error: invalid day value")
	} //if

	// Build and make the request
	data, err := makeRequest(dailyLocFmt.FormatLoc(latitude, longitude, days, a.key))

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	return unmarshalDaily(data)
} //DailyByLoc

func makeRequest(reqURL string) ([]byte, error) {
	// Make the HTTP Request
	resp, err := http.Get(reqURL)

	// Check for the error
	if err != nil {
		return nil, err
	} //if

	// Check the response code
	if code := resp.StatusCode; code != http.StatusOK {
		return nil, fmt.Errorf("request error: bad status %d", code)
	} //if

	// Create a buffer
	var buff bytes.Buffer

	// Copy to the buffer
	_, err = io.Copy(&buff, resp.Body)

	// Check for the error
	if err != nil {
		return nil, err
	} //if

	return buff.Bytes(), nil
} //makeRequest
