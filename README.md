#OpenWeatherMap

[![GoDoc](https://godoc.org/github.com/antizealot1337/openweathermap?status.svg)](https://godoc.org/github.com/antizealot1337/openweathermap)

An unofficial Go library and simple command line tool for accessing weather data
from http://openweathermap.org.

##Library Usage
Register with Open Weather Map at
http://http://home.openweathermap.org/users/sign_up. To build the command line
application create a file with the key as follows.

```Go
package openweathermap

var APIKey = "Your api key"
```

A good file name is ```key.go``` since that file will be ignored by git anyway.

## Command line usage
The command line program can be used to get the forecast (hourly or daily) by
latitude and longitude or city and country. The country must be an ISO 3166
country code (i.e. "US").

To get the forecast for a specific location (will still be relative) use

```
forecast -lat <latitude> -lon <longitude>
```

To get the forecast for a city use

```
forecast -city <city> -country <ISO 3166 country code>
```

You can specify the number of days for the daily for cast with the ```-days
<number of days>```. The values must be between 1 and 16.

If you want forecasts
that span 3 hour time frames use the ```-hourly``` flag.

The degree can be displayed in Kelvin, Celcius, or Fahrenheit. It defaults to
Celcius but can be changed with the ```-degrees <abbreviation>``` flag.

##License

This software is licensed under the MIT License. For more information please
view the LICENSE file.
